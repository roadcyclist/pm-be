module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'object-curly-spacing': [2, 'always'],
    'no-template-curly-in-string': 0,
    'prefer-promise-reject-errors': 0,
    'no-throw-literal': 0,
    'padding-line-between-statements': [
      'error',
      { blankLine: 'always', prev: 'block-like', next: '*' },
      { blankLine: 'always', prev: '*', next: 'block-like' },
      { blankLine: 'always', prev: '*', next: 'return' }
    ]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
