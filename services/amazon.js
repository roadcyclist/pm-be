const multer = require('multer')
const multerS3 = require('multer-s3')
const AWS = require('aws-sdk')
const path = require('path')
const crypto = require('crypto')

const BUCKET_NAME = process.env.AWS_BUCKET_NAME

AWS.config.update({
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  region: 'us-east-1'
});

const s3 = new AWS.S3()

exports.deleteObject = function (key) {
  const params = { Bucket: BUCKET_NAME, Key: key }

  return s3.deleteObject(params).promise()
}

exports.getObject = function (key) {
  const params = { Bucket: BUCKET_NAME, Key: key }

  return s3.getObject(params).promise()
}

exports.upload = multer({
  storage: multerS3({
    s3,
    bucket: BUCKET_NAME,
    ACL: 'public-read',
    contentType (req, file, cb) {
      cb(null, file.mimetype)
    },
    key (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)

        cb(null, raw.toString('hex') + path.extname(file.originalname))
      })
    }
  }),
  limits: {
    fieldSize: '5MB',
    fileSize: '5MB'
  },
  fileFilter (req, file, cb) {
    if (file.mimetype.includes('image')) {
      cb(null, true)
    } else {
      cb(null, false)
    }
  }
}).single('file')
