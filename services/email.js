const { getObject } = require('./amazon')
const mailjet = require('node-mailjet').connect(process.env.MAILJET_KEY, process.env.MAILJET_SECRET)

const TEMPLATES = {
  confirmEmail: {
    TemplateID: 689513,
    Subject: 'Confirm your email'
  },
  welcomeEmail: {
    TemplateID: 628981,
    Subject: 'Welcome to CryptoVeil'
  },
  rejectedEmail: {
    TemplateID: 629277,
    Subject: 'You have been rejected'
  },
  passwordEmail: {
    TemplateID: 629301,
    Subject: 'Your new password'
  },
  contactMeEmail: {
    TemplateID: 681807,
    Subject: 'Contact Me'
  },
  awaitingApprovalEmail: {
    TemplateID: 681895,
    Subject: 'Awaiting Approval'
  },
  needDataEmail: {
    TemplateID: 686144,
    Subject: 'Purchased Need'
  },
  needPurchasedEmail: {
    TemplateID: 682004,
    Subject: 'Your need has been purchased'
  }
}

const generateEmailRequest = (to, template, options = { Variables: {}, Attachments: [] }) => {
  return mailjet
    .post('send', { 'version': 'v3.1' })
    .request({
      Messages: [
        {
          From: {
            Email: process.env.ADMIN_EMAIL,
            Name: 'CryptoVeil'
          },
          To: [
            {
              Email: to
            }
          ],
          TemplateLanguage: true,
          TemplateErrorReporting: {
            Email: process.env.ADMIN_EMAIL,
            Name: 'Template Error'
          },
          ...template,
          ...options
        }
      ]
    })
}

const sendConfirmEmail = async (address, userId, activationId) => {
  const activationUrl = `${process.env.API_URL}/user/activate?userId=${userId}&activationId=${activationId}`
  const link = `<a href="${activationUrl}">Activate account</a>`

  const request = generateEmailRequest(
    address,
    TEMPLATES.confirmEmail,
    { Variables: { link } }
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

const sendWelcomeEmail = async (user) => {
  const request = generateEmailRequest(
    user.email,
    TEMPLATES.welcomeEmail,
    { Variables: { name: user.name || user.firstName, type: user.userRole } }
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

const sendUserRejectedEmail = async (user) => {
  const request = generateEmailRequest(
    user.email,
    TEMPLATES.rejectedEmail
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

const sendPasswordRecoveryEmail = async (email, password) => {
  const request = generateEmailRequest(
    email,
    TEMPLATES.passwordEmail,
    { Variables: { password } }
  )

  try {
    await request
    console.log('sent')
  } catch (e) {
    console.error(e)
  }
}

const sendContactUsEmail = async (name, email, text) => {
  const request = generateEmailRequest(
    process.env.ADMIN_EMAIL,
    TEMPLATES.contactMeEmail,
    {
      Variables: {
        name,
        email,
        text
      }
    }
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

const sendAwaitingApprovalEmail = async (name) => {
  const request = generateEmailRequest(
    process.env.ADMIN_EMAIL,
    TEMPLATES.awaitingApprovalEmail,
    {
      Variables: { name }
    }
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

const sendNeedPurchasedEmail = async (user, need) => {
  const request = generateEmailRequest(
    user.email,
    TEMPLATES.needPurchasedEmail,
    {
      Variables: {
        needTitle: need.title,
        needPrice: need.price,
        name: user.firstName
      }
    }
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

const sendNeedDataEmail = async (buyer, need, needData) => {
  let promises = []
  let phoneNumber = ''

  needData.forEach(async ({ type, value }, index) => {
    if (type === 'Phone number') {
      phoneNumber = value
    } else {
      const key = value.split('.com/')[1]
      promises.push(getObject(key))
    }
  })

  const response = await Promise.all(promises)
  const attachments = response.map(({ ContentType, Body }, index) => {
    const fileExtension = ContentType.split('/')[1]

    return {
      ContentType: ContentType,
      Base64Content: Body.toString('base64'),
      Filename: `file${index}.${fileExtension}`
    }
  })

  console.log(attachments)

  const request = generateEmailRequest(
    buyer.email,
    TEMPLATES.needDataEmail,
    {
      Variables: { phoneNumber, name: buyer.name },
      Attachments: attachments
    }
  )

  try {
    await request
  } catch (e) {
    console.error(e)
  }
}

module.exports = {
  sendConfirmEmail,
  sendWelcomeEmail,
  sendPasswordRecoveryEmail,
  sendContactUsEmail,
  sendNeedDataEmail,
  sendNeedPurchasedEmail,
  sendUserRejectedEmail,
  sendAwaitingApprovalEmail
}
