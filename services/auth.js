const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const models = require('../db/models/index')
const { sendConfirmEmail } = require('./email')
const SECRET = process.env.TOKEN_SECRET

let admin = {
  id: 0,
  email: process.env.ADMIN_EMAIL,
  password: process.env.ADMIN_PASSWORD,
  refreshTokens: []
}

function getAdminData () {
  return admin
}

function setRefreshTokens (tokens) {
  if (admin.refreshTokens.length > 5) {
    admin.refreshTokens = [tokens[tokens.length - 1]] // remove all tokens but current
  } else {
    admin = { ...admin, refreshTokens: tokens }
  }
}

const generateTokens = async (user, isAdmin = false) => {
  const nowUnix = Math.floor(Date.now() / 1000)
  const payload = {
    id: user.id?user.id:user.user_id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  const refreshToken = jwt.sign({
    ...payload,
    exp: nowUnix + +process.env.REFRESH_EXPIRES_IN
  }, SECRET)

  const accessToken = jwt.sign({
    ...payload,
    exp: nowUnix + +process.env.ACCESS_EXPIRES_IN
  }, SECRET)

  if (!isAdmin) {
    await user.update({ refreshToken })
  }

  return {
    refreshToken,
    accessToken,
    expiresIn: nowUnix + +process.env.ACCESS_EXPIRES_IN
  }
}

const verifyToken = (exceptionUrls = [], adminRequired = false) => (req, res, next) => {
  if (exceptionUrls.includes(req.url)) {
    next()
  } else {
    let token = req.header('Authorization')

    if (token && token.includes('Bearer')) {
      token = token.split('Bearer ')[1]
    }

    jwt.verify(token, SECRET, (err, decoded) => {
      if (err) {
        return res.status(400).json(err)
      }

      // Set user data to the next response
      res.locals.user = decoded

      if (adminRequired && !decoded.isAdmin) {
        return res.status(403).send({ error: 'Requires admin rights' })
      }

      next()
    })
  }
}

const createActivation = async (req, user) => {
  console.log(user);
  const activation = await models.Activation.create({
    userId: user.user_id?user.user_id:user.id,
    activationId: crypto.randomBytes(20).toString('hex'),
    userRole: user.userRole
  })
  console.log("createAct");
  await sendConfirmEmail(req.body.email, user.id, activation.activationId)
}

module.exports = {
  generateTokens,
  verifyToken,
  getAdminData,
  setRefreshTokens,
  createActivation
}
