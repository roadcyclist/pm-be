FROM node:10.13

WORKDIR /data

EXPOSE 3000

CMD npm install && npm start
