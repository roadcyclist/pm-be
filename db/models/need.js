'use strict'

module.exports = (sequelize, DataTypes) => {
  const Need = sequelize.define('Need', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    documents: {
      type: DataTypes.TEXT,
      get: function() {
        return JSON.parse(this.getDataValue("documents"));
      },
      set: function(value) {
        return this.setDataValue("documents", JSON.stringify(value));
      }
    },
    location: DataTypes.STRING,
    category: DataTypes.STRING,
    price: DataTypes.FLOAT
  },{tableName:'Wo_Needs'})

  Need.associate = (models) => {
    Need.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE'
    })
  }

  return Need
}
