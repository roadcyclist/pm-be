'use strict'

module.exports = (sequelize, DataTypes) => {
  const Business = sequelize.define('Business', {
    avatar: DataTypes.STRING,
    name: DataTypes.STRING,
    orgName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    address: DataTypes.STRING,
    emailConfirmed: { type: DataTypes.BOOLEAN, defaultValue: false },
    needsInCart: {
      type: DataTypes.TEXT,
      get: function() {
        return JSON.parse(this.getDataValue("needsInCart"));
      },
      set: function(value) {
        return this.setDataValue("needsInCart", JSON.stringify(value));
      }
    },
    boughtNeedIds: { 
      type: DataTypes.TEXT,
      get: function() {
        return JSON.parse(this.getDataValue("boughtNeedIds"));
      },
      set: function(value) {
        return this.setDataValue("boughtNeedIds", JSON.stringify(value));
      }
    },
    refreshToken: DataTypes.STRING,
    userRole: DataTypes.STRING,
    isDeleted: { type: DataTypes.BOOLEAN, defaultValue: false }
  },{modelName:'Wo_Businesses',tableName:'Wo_Businesses'})

  return Business
}
