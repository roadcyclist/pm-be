'use strict'

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    user_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    avatar: DataTypes.STRING,
    confirmed: { type: DataTypes.BOOLEAN, defaultValue: false },
    confirmationImage: DataTypes.STRING,
    utilityBills: DataTypes.STRING,
    driverLicense: DataTypes.STRING,
    idCard: DataTypes.STRING,
    phone_number: DataTypes.STRING,
    birthday: DataTypes.DATE,
    userRole: DataTypes.STRING,
    isDeleted: { type: DataTypes.BOOLEAN, defaultValue: false },
    admin: { type: DataTypes.ENUM('0','1'), defaultValue: '0' },
    emailConfirmed: { type: DataTypes.BOOLEAN, defaultValue: false },
    balance: { type: DataTypes.FLOAT, defaultValue: 0 },
    refreshToken: DataTypes.STRING
  },{modelName:'Wo_Users',tableName:'Wo_Users'})

  return User
}
