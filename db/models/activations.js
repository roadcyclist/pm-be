'use strict'

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('Activation', {
    userId: DataTypes.INTEGER,
    activationId: DataTypes.STRING,
    userRole: DataTypes.STRING
  },{tableName:'Wo_Activations'})
}
