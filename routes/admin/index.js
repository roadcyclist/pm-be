const express = require('express')
const router = express.Router()

const auth = require('./auth')
const users = require('./users')
const staticRoutes = require('./static')

router.use('/auth', auth)
router.use('/', users)
router.use('/static', staticRoutes)

module.exports = router
