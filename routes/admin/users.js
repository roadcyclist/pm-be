const express = require('express')
const models = require('../../db/models')
const { verifyToken } = require('../../services/auth')
const { sendWelcomeEmail, sendUserRejectedEmail } = require('../../services/email')
const Joi = require('joi')

const router = express.Router()
const userSchema = Joi.object().keys({
  emailConfirmed: Joi.boolean().optional(),
  confirmed: Joi.boolean().optional(),
  page: Joi.number().greater(0).required(),
  size: Joi.number().required()
})

router.use(verifyToken([], true))

/**
 * @api {post} /admin/users/all Get User List
 * @apiName Get Users
 * @apiGroup Admin
 *
 * @apiParam {Number} page Page to view. Starts with 1.
 * @apiParam {Number} size Amount of records per page.
 * @apiParam {Boolean} [emailConfirmed] Get only activated/not activated users.
 * @apiParam {Boolean} [confirmed] Get users, confirmed by admin.
 *
 * @apiSuccess {Object} pagination Pagination Info -> page, size, total
 * @apiSuccess {Array} data Array of users
 * @apiPermission Admin
 */
router.post('/users/all', async (req, res) => {
  try {
    await Joi.validate(req.body, userSchema)

    let where = {
      isDeleted: false
    }

    if (req.body.emailConfirmed !== undefined) {
      where.emailConfirmed = req.body.emailConfirmed
    }

    if (req.body.confirmed !== undefined) {
      where.confirmed = req.body.confirmed
    }

    const { count: total, rows: data } = await models.User.findAndCountAll({
      limit: req.body.size,
      offset: req.body.size * (req.body.page - 1),
      where
    })

    const keysToRemove = ['password', 'refreshToken', 'updatedAt', 'userRole']
    let payload = data.map(user => {
      keysToRemove.forEach(key => delete user.dataValues[key])

      return user
    })

    res.send({
      pagination: {
        total,
        size: req.body.size,
        page: req.body.page
      },
      data: payload
    })
  } catch (e) {
    res.status(400).send(e)
  }
})

const getUserNeedsSchema = Joi.object().keys({
  page: Joi.number().integer().required(),
  size: Joi.number().integer().required()
})

/**
 * @api {post} /admin/users/:userId/needs Get User Needs
 * @apiName Get User Needs
 * @apiGroup Admin
 *
 * @apiParam (Pagination) {Number} page Pagination page
 * @apiParam (Pagination) {Number} size Pagination size
 *
 * @apiSuccess {Array} needs
 * @apiSuccess {Object} pagination
 * @apiPermission Admin
 */
router.post('/users/:userId/needs', async (req, res) => {
  try {
    await Joi.validate(req.body, getUserNeedsSchema)
  } catch (e) {
    return res.status(400).send(e)
  }

  const { size, page } = req.body

  const { count, rows: data } = await models.Need
    .findAndCountAll({
      limit: size,
      offset: size * (+page - 1),
      where: { user_id: req.params.userId }
    })

  if (!data) {
    res.status(404).send()
  } else {
    res.json({
      data,
      pagination: {
        total: count,
        size,
        page
      }
    })
  }
})

const businessSchema = Joi.object().keys({
  page: Joi.number().greater(0).required(),
  size: Joi.number().required(),
  emailConfirmed: Joi.boolean().optional()
})

/**
 * @api {post} /admin/businesses/all Get Business List
 * @apiName Get Businesses
 * @apiGroup Admin
 *
 * @apiParam {Number} page Page to view. Starts with 1.
 * @apiParam {Number} size Amount of records per page.
 * @apiParam {Boolean} [emailConfirmed] Get only activated/not activated users.
 *
 * @apiSuccess {Object} pagination Pagination Info
 * @apiSuccess {Array} data Array of users
 * @apiPermission Admin
 */
router.post('/businesses/all', async (req, res) => {
  try {
    await Joi.validate(req.body, businessSchema)

    let where = {
      isDeleted: false
    }

    if (req.body.emailConfirmed !== undefined) {
      where.emailConfirmed = req.body.emailConfirmed
    }

    const { count: total, rows: data } = await models.Business.findAndCountAll({
      limit: req.body.size,
      offset: req.body.size * (req.body.page - 1),
      where
    })

    const keysToRemove = ['password', 'refreshToken', 'updatedAt', 'userRole', 'needsInCart']
    let payload = data.map(business => {
      keysToRemove.forEach(key => delete business.dataValues[key])

      return business
    })

    res.send({
      pagination: {
        total,
        size: req.body.size,
        page: req.body.page
      },
      data: payload
    })
  } catch (e) {
    res.status(400).send(e)
  }
})

async function deleteByType (req, res, type) {
  try {
    let query;
    if(type == "User"){
      query = { where : {user_id: req.params.id}};
    }else{
      query = { where : {id: req.params.id}};
    }
     
    const data = await models[type].findOne(query)

    if (data.isDeleted) return res.status(400).send({ error: 'User has already been deleted' })
    if (!data) return res.status(404).send({ error: 'User not found' })

    let cleanData = { ...data.dataValues }

    for (const key in cleanData) {
      if (key !== 'email') {
        cleanData[key] = null
      }
    }

    cleanData.isDeleted = true

    await data.update({ ...cleanData })
    res.status(200).send(data)
  } catch (e) {
    res.status(500).send(e)
  }
}

/**
 * @api {delete} /admin/:type/:id Delete User/Business
 * @apiName Delete User/Business
 * @apiGroup Admin
 *
 * @apiParam {String} type 'users' or 'businesses'
 * @apiParam {Number} id Id to be deleted
 *
 * @apiPermission Admin
 */
router.delete('/users/:id', (req, res) => {
  deleteByType(req, res, 'User')
})

router.delete('/businesses/:id', (req, res) => {
  deleteByType(req, res, 'Business')
})

/**
 * @api {post} /admin/users/:id/approve Approve User
 * @apiName Approve User
 * @apiGroup Admin
 *
 * @apiPermission Admin
 */
router.post('/users/:id/approve', async (req, res) => {
  const user = await models.User.findOne({
    where: { user_id: req.params.id }
  })

  if (!user) return res.status(400).send({ error: 'User not found' })
  user.confirmed = true
  await user.save()

  try {
    await sendWelcomeEmail(user)
    res.send()
  } catch (e) {
    res.status(500).send(e)
  }
})

/**
 * @api {post} /admin/users/:id/reject Reject User
 * @apiName Reject User
 * @apiGroup Admin
 *
 * @apiPermission Admin
 */
router.post('/users/:id/reject', async (req, res) => {
  const user = await models.User.findOne({
    where: { user_id: req.params.id }
  })

  if (!user) return res.status(400).send({ error: 'User not found' })

  try {
    await user.update({ isDeleted: true })
    await sendUserRejectedEmail(user)
    res.send()
  } catch (e) {
    res.status(500).send(e)
  }
})

/**
 * @api {get} /admin/users/:id Get User
 * @apiName Get User
 * @apiGroup Admin
 *
 * @apiPermission Admin
 */
router.get('/users/:id', async (req, res) => {
  const userId = req.params.id
  const isNumber = !isNaN(Number(userId))

  if (!userId || !isNumber) {
    return res.send(400).send({ error: 'Wrong user id' })
  }

  try {
    const user = await models.User.findOne({ where: { user_id: userId } })
    const payload = { ...user.dataValues }

    delete payload.password
    delete payload.isAdmin

    res.send(payload)
  } catch (e) {
    res.status(500).send(e)
  }
})

/**
 * @api {delete} /admin/needs/:needId Delete a need
 * @apiName Delete Need
 * @apiGroup Admin
 *
 * @apiPermission Admin
 */
router.delete('/needs/:needId', async (req, res) => {
  try {
    const need = await models.Need.findOne({
      where: { id: req.params.needId }
    })

    if (!need) {
      return res.status(404).send({ error: 'This need does not exist' })
    }

    await need.destroy()
    res.status(200).send({ message: 'Successfully removed' })
  } catch (e) {
    res.status(400).send(e)
  }
})

module.exports = router
