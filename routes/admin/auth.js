const express = require('express')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const { generateTokens, verifyToken, getAdminData, setRefreshTokens } = require('../../services/auth')

const router = express.Router()
router.use(verifyToken(['/login', '/refresh'], true))

const loginSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required()
})

/**
 * @api {post} /admin/auth/login Login
 * @apiName Login Admin
 * @apiGroup Admin
 *
 * @apiParam {String} email
 * @apiParam {String} password
 *
 * @apiSuccess {String} accessToken
 * @apiSuccess {String} refreshToken
 * @apiSuccess {Number} expiresIn Access token expiration time in seconds
 *
 * @apiError {String} error Error Message
 */
router.post('/login', async (req, res) => {
  const admin = getAdminData()

  try {
    await Joi.validate(req.body, loginSchema)

    if (req.body.email !== admin.email ||
      req.body.password !== admin.password) {
      throw 'Invalid credentials'
    }

    const authData = await generateTokens({
      id: admin.id,
      email: admin.email,
      isAdmin: true
    }, true)

    setRefreshTokens([...admin.refreshTokens, authData.refreshToken])

    res.send(authData)
  } catch (e) {
    return res.status(400).send({ error: e })
  }
})

/**
 * @api {post} /admin/auth/refresh Refresh admin token
 * @apiName Refresh Token
 * @apiGroup Admin
 * @apiPermission Admin
 *
 * @apiParam {String} refreshToken token
 *
 * @apiHeader {String} Authorization accessToken
 *
 * @apiSuccess {String} accessToken
 * @apiSuccess {String} refreshToken
 * @apiSuccess {Number} expiresIn Access token expiration time in seconds
 */
router.post('/refresh', async (req, res) => {
  const admin = getAdminData()

  try {
    await Joi.validate(req.body.refreshToken, Joi.string().required())

    if (!admin.refreshTokens.includes(req.body.refreshToken)) throw { error: 'Invalid token' }

    jwt.verify(req.body.refreshToken, process.env.TOKEN_SECRET, async (err, decoded) => {
      if (err) return res.status(401).send(err)

      if (decoded.isAdmin && decoded.email === admin.email) {
        const authData = await generateTokens({
          id: admin.id,
          email: admin.email,
          isAdmin: true
        }, true)

        let tokens = [...admin.refreshTokens]
        const oldRefreshIndex = tokens.findIndex(t => t === req.body.refreshToken)
        tokens.splice(oldRefreshIndex, 1, authData.refreshToken) // replace old token with the new one
        setRefreshTokens(tokens)

        res.send(authData)
      } else res.status(500).send()
    })
  } catch (e) {
    res.status(401).send(e)
  }
})

/**
 * @api {post} /admin/auth/logout Logout
 * @apiName Logout Admin
 * @apiPermission Admin
 * @apiGroup Admin
 * @apiHeader {String} Authorization accessToken
 *
 */
router.post('/logout', async (req, res) => {
  setRefreshTokens([])

  res.status(200).send()
})

module.exports = router
