const express = require('express')
const fs = require('fs')
const path = require('path')
const Joi = require('joi')
const { verifyToken } = require('../../services/auth')
const FILE_PATH = path.join(__dirname, '../../static.json')

const router = express.Router()
router.use(verifyToken([], true))

/**
 * @api {get} /admin/static/:type Get Static Content
 * @apiName Get Static Content
 * @apiGroup Admin
 * @apiPermission Admin
 * @apiHeader Authorization accessToken
 *
 * @apiParam type {String} landing/privacy/terms
 */
router.get('/:type', async (req, res) => {
  try {
    await Joi.validate(req.params.type, Joi.string().valid('landing', 'privacy', 'terms'))

    if (req.params.type === 'landing') {
      return fs.readFile(
        path.join(__dirname, '../../static.json'),
        (e, data) => {
          if (e) res.status(500).send()

          return res.send(data)
        })
    }

    res.sendFile(path.join(__dirname, `../../views/${req.params.type}.html`))
  } catch (error) {
    res.status(400).send({ error })
  }
})

/**
 * @api {post} /admin/static Create/Update Static Content
 * @apiName Create/Update Static Content
 * @apiGroup Admin
 *
 * @apiParam {Object} data Static data
 * @apiParam {String} type landing/privacy/terms
 *
 * @apiPermission Admin
 *
 */
const postStaticSchema = Joi.object().keys({
  type: Joi.string().valid('landing'),
  data: Joi.object().required()
})

router.post('/', async (req, res) => {
  try {
    await Joi.validate(req.body, postStaticSchema)

    fs.writeFile(
      FILE_PATH,
      JSON.stringify(req.body.data, null, 2),
      (err) => {
        if (err) return res.status(500).send(err)
        res.send()
      })
  } catch (error) {
    res.status(400).send({ error })
  }
})

/**
 * @api {patch} /admin/static/fee Update Fee
 * @apiName Update Service Fee
 * @apiGroup Admin
 *
 * @apiParam {Number} amount New percentage value
 *
 * @apiPermission Admin
 *
 */
router.patch('/fee', async (req, res) => {
  try {
    await Joi.validate(req.body.amount, Joi.number().min(0).less(100).required())

    const fileLocation = path.join(__dirname, '../../static.json')
    fs.readFile(fileLocation, (e, data) => {
      if (e) res.status(500).send()

      let parsedData = JSON.parse(data)
      parsedData.fee = req.body.amount
      fs.writeFile(fileLocation, JSON.stringify(parsedData, null, 2), e => {
        if (e) res.status(500).send(e)

        res.send({ message: 'Fee changed successfully' })
      })
    })
  } catch (e) {
    res.status(400).send(e)
  }
})

module.exports = router
