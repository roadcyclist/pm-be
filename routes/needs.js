const express = require('express')
const sequelize = require('sequelize')
const Joi = require('joi')

const Op = sequelize.Op
const router = express.Router()

const models = require('../db/models/index')
const auth = require('../services/auth')

router.use(auth.verifyToken())

const createNeedValidationSchema = Joi.object().keys({
  title: Joi.string().max(60).required(),
  description: Joi.string().max(350).allow(''),
  userId: Joi.number().integer().required(),
  documents: Joi.array().items(Joi.string()).required(),
  category: Joi.string().required(),
  location: Joi.string().required(),
  price: Joi.number().required()
})

/**
 * @api {post} /needs Create a need
 * @apiName Create Need
 * @apiGroup Needs
 *
 * @apiParam {String} title Need name
 * @apiParam {String} [description]
 * @apiParam {Number} userId
 * @apiParam {String[]} documents Document Name List
 * @apiParam {String} category Category Name
 * @apiParam {String} location
 * @apiParam {Number} price
 *
 * @apiSuccess {String} message
 *
 * @apiPermission Authorized user
 */
router.post('/', async (req, res) => {
  try {
    await Joi.validate(req.body, createNeedValidationSchema)

    if (res.locals.user.id !== req.body.userId) {
      return res.status(403).send()
    }

    const uniqueFields = {
      location: req.body.location,
      category: req.body.category,
      documents: req.body.documents.sort(),
      user_id: req.body.userId
    }

    const need = await models.Need.findOne({
      where: { ...uniqueFields }
    })

    if (need) {
      throw { error: 'This need has already been created by you' }
    }

    await models.Need.create({
      ...uniqueFields,
      title: req.body.title,
      description: req.body.description,
      price: req.body.price
    })

    res.status(200).json({ message: 'Need has been created successfully!' })
  } catch (e) {
    res.status(400).send(e)
  }
})

const getBusinessNeedsSchema = Joi.object().keys({
  size: Joi.number().integer().required(),
  page: Joi.number().integer().required(),
  categories: Joi.array().items(Joi.string()).optional(),
  documents: Joi.array().items(Joi.string()).optional(),
  location: Joi.array().items(Joi.string()).optional()
})

/**
 * @api {post} /needs/business/:businessId Get Needs For Business
 * @apiName Get Needs For Business
 * @apiGroup Needs
 *
 * @apiParam (Pagination) {Number} page Pagination page
 * @apiParam (Pagination) {Number} size Pagination size
 * @apiParam (Filter) {String[]} [documents]
 * @apiParam (Filter) {String} [category]
 * @apiParam (Filter) {String[]} [location]
 *
 * @apiPermission Authorized user
 */
router.post('/business/:businessId', async (req, res) => {
  try {
    await Joi.validate(req.body, getBusinessNeedsSchema)

    if (res.locals.user.id !== +req.params.businessId) {
      return res.status(403).send()
    }

    const { size, page, categories, location, documents } = req.body
    const where = {}
    
    const business = await models.Business.findOne({ where: { id: req.params.businessId } })

    if (!business) {
      return res.status(404).send({ error: 'There is no business with such id' })
    }

    if (business.boughtNeedIds && business.boughtNeedIds.length) {
      where.id = {
        [Op.notIn]: business.boughtNeedIds
      }
    }

    if (location) {
      where.location = location
    }

    if (categories) {
      where.category = {
        [Op.in]: categories
      }
    }

    if (documents && documents.length) {
      where.documents = {
        [Op.contains]: documents,
        [Op.contained]: documents
      }
    }

    const { count, rows: data } = await models.Need.findAndCountAll({
      limit: size,
      offset: size * (+page - 1),
      where,
      order: [
        ['createdAt', 'DESC']
      ]
    })
    if (data) {
      res.json({
        needs: data,
        total: count
      })
      
    } else {
      res.status(404).send()
    }
  } catch (e) {
    res.status(400).send(e.message || e)
  }
})

/**
 * @api {get} /needs/:needId Get Need
 * @apiName Get Need
 * @apiGroup Needs
 * @apiPermission Authorized user
 */
router.get('/:id', async (req, res) => {
  try {
    await Joi.validate(req.params.id, Joi.number().required())

    const need = await models.Need.findOne({
      where: { id: req.params.id }
    })

    if (!need) res.status(404).send({ error: 'This need does not exist' })

    if (res.locals.user.id !== need.user_id) {
      return res.status(403).send()
    }

    res.send(need)
  } catch (e) {
    res.status(400).send(e)
  }
})

const getUserNeedsSchema = Joi.object().keys({
  page: Joi.number().integer().required(),
  size: Joi.number().integer().required()
})

/**
 * @api {post} /needs/user/:userId Get User Needs
 * @apiName Get User Needs
 * @apiGroup Needs
 *
 * @apiParam (Pagination) {Number} page Pagination page
 * @apiParam (Pagination) {Number} size Pagination size
 *
 * @apiPermission Authorized user
 */
router.post('/user/:userId', async (req, res) => {
  try {
    await Joi.validate(req.body, getUserNeedsSchema)
  } catch (e) {
    return res.status(400).send(e)
  }

  if (res.locals.user.id !== +req.params.userId) {
    return res.status(403).send()
  }

  const { size, page } = req.body

  const { count, rows: data } = await models.Need
    .findAndCountAll({
      limit: size,
      offset: size * (+page - 1),
      where: { user_id: req.params.userId },
      order: [
        ['createdAt', 'DESC']
      ]
    })
    
  if (!data) {
    res.status(404).send()
  } else {
    res.json({
      needs: data,
      total: count
    })
  }
})

/**
 * @api {delete} /needs/:needId Delete a need
 * @apiName Delete Need
 * @apiGroup Needs
 *
 * @apiPermission Authorized user
 */
router.delete('/:needId', async (req, res) => {
  try {
    await Joi.validate(req.params.needId, Joi.string().required())

    const need = await models.Need.findOne({
      where: { id: req.params.needId }
    })

    if (res.locals.user.id !== need.user_id) {
      return res.status(403).send({ error: 'It is not your need' })
    }

    if (!need) {
      return res.status(404).send({ error: 'This need does not exist' })
    }

    await need.destroy()
    res.status(200).send({ message: 'Successfully removed' })
  } catch (e) {
    res.status(400).send({ error: e })
  }
})

module.exports = router
