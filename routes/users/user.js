const express = require('express')
const models = require('../../db/models/index')
const auth = require('../../services/auth')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const Joi = require('joi')

const router = express.Router()

router.use(auth.verifyToken())

function encrypt(text,secret) {
  if (process.versions.openssl <= '1.0.1f') {
      throw new Error('OpenSSL Version too old, vulnerability to Heartbleed')
  }

  console.log(process.env.PASSWORD_IV_LENGTH);
  let iv = crypto.randomBytes(16);
  console.log(iv.toString('hex'));
  console.log(process.env.PASSWORD_AES_METHOD)
  let cipher = crypto.createCipheriv(process.env.PASSWORD_AES_METHOD, new Buffer(secret), iv);
  let encrypted = cipher.update(text);

  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text,secret) {
  let textParts = text.split(':');
  let iv = new Buffer(textParts.shift(), 'hex');
  let encryptedText = new Buffer(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(process.env.PASSWORD_SECRET), iv);
  let decrypted = decipher.update(encryptedText);

  decrypted = Buffer.concat([decrypted, decipher.final()]);

  return decrypted.toString();
}

const updateBusinessSchema = Joi.object().keys({
  name: Joi.string().required()
})

/**
 * @api {patch} /user/b/:businessId Update Business
 * @apiName Update Business
 * @apiGroup Users
 *
 * @apiParam {String} name User's name
 *
 * @apiPermission Authorized
 */
router.patch('/b/:businessId', async (req, res) => {
  try {
    await Joi.validate(req.body, updateBusinessSchema)
  } catch (error) {
    return res.status(400).send({ error })
  }

  if (res.locals.user.id !== +req.params.businessId) {
    return res.status(403).send()
  }

  const business = await models.Business.findOne({
    where: { id: req.params.businessId }
  })

  if (!business) {
    return res.status(404).send({ error: 'This user is not found' })
  }

  try {
    await business.update({ name: req.body.name })
    res.send()
  } catch (error) {
    res.status(500).send(error)
  }
})

const passwordSchema = Joi.object().keys({
  email: Joi.string().email(),
  password: Joi.string().required(),
  oldPassword: Joi.string().required()
})

/**
 * @api {patch} /user/:userId/password Update Password
 * @apiName Update Password
 * @apiGroup Auth
 *
 * @apiParam {String} email User email
 * @apiParam {String} oldPassword
 * @apiParam {String} password new password
 *
 * @apiPermission Authorized
 */
router.patch('/:userId/password', async (req, res) => {
  try {
    await Joi.validate(req.body, passwordSchema)
  } catch (error) {
    return res.status(400).send({ error })
  }
  if (res.locals.user.id !== +req.params.userId) {
    return res.status(403).send()
  }
  try {
    //const params = { where: { email: req.body.email, id: req.params.userId } }

    const [user, business] = await Promise.all([
      models.User.findOne({ where: { email: req.body.email, user_id: req.params.userId }}),
      models.Business.findOne({ where: { email: req.body.email, id: req.params.userId }})
    ])

    const person = user || business

    if (!person) {
      return res.status(400).send({ error: 'E-mail is not found' })
    }
    let oldPassword = decrypt(person.password);
    if (!(req.body.oldPassword === oldPassword)) {
      return res.status(400).send({ error: 'Wrong password' })
    }
    let newPassHash = encrypt(req.body.password,process.env.PASSWORD_SECRET);
    await person.update({ password: newPassHash })
    res.send({ message: 'Password successfully changed!' })
  } catch (error) {
    res.status(500).send({ error })
  }
})

/**
 * @api {get} /user/:userId Get User
 * @apiName Get User
 * @apiGroup Users
 *
 * @apiPermission Authorized
 */
router.get('/:id', async (req, res) => {
  const user = await models.User.findOne({
    where: { user_id: req.params.id }
  })

  if (!user) {
    return res.status(404).send({ error: 'User not found' })
  }

  const payload = { ...user.dataValues }
  delete payload.password
  delete payload.refreshToken

  res.send(payload)
})

/**
 * @api {get} /user/b/:businessId Get Business
 * @apiName Get Business
 * @apiGroup Users
 *
 * @apiPermission Authorized
 */
router.get('/b/:id', async (req, res) => {
  const business = await models.Business.findOne({
    where: { id: req.params.id }
  })

  if (!business) {
    return res.status(404).send({ error: 'User not found' })
  }

  const payload = { ...business.dataValues }
  delete payload.password
  delete payload.refreshToken

  res.send(payload)
})

module.exports = router
