const express = require('express')
const Joi = require('joi')
const router = express.Router()
const models = require('../../db/models')
const { upload, deleteObject } = require('../../services/amazon')
const auth = require('../../services/auth')
const { sendAwaitingApprovalEmail } = require('../../services/email')

router.use(auth.verifyToken())

const uploadDocumentSchema = Joi.object().keys({
  type: Joi.string().valid('confirmationImage', 'avatar'),
  userId: Joi.number().greater(0).required()
})

/**
 * @api {post} /uploads/:type/:userId Upload Document By Type
 * @apiName Upload Document By Type
 * @apiGroup Uploads
 *
 * @apiParam {String} type confirmationImage or avatar. Query param!
 *
 * @apiPermission Authorized
 */
router.post('/:type/:userId', async (req, res) => {
  try {
    await Joi.validate(req.params, uploadDocumentSchema)
  } catch (error) {
    return res.status(400).send(error)
  }
  console.log(res.locals.user.id);
  if (res.locals.user.id !== +req.params.userId) {
    return res.status(403).send()
  }

  try {
    upload(req, res, async (err) => {
      if (err) throw err
      let where = {};
      if (req.body.type == 'Business'){
        where = { id: req.params.userId }
      }else{
        where = { user_id: req.params.userId }
      }
      const result = await models[req.body.type].findOne({
        where
      })
      await result.update({
        [req.params.type]: req.file.location
      })
      if (req.params.type === 'confirmationImage') {
        await sendAwaitingApprovalEmail(`${result.firstName} ${result.lastName}`)
      }
      let resultData = {...result}
      const keysToRemove = ['password', 'createdAt', 'updatedAt']
      keysToRemove.forEach(key => delete resultData[key])
      res.status(200).send(resultData);
    })
  } catch (error) {
    res.status(500).send({ error })
  }
})

const deleteAvatarSchema = Joi.object().keys({
  userId: Joi.number().greater(0).required()
})

/**
 * @api {delete} /uploads/avatar/:userId Delete User Avatar
 * @apiName Delete User Avatar
 * @apiGroup Uploads
 *
 * @apiPermission Authorized
 */
router.delete('/avatar/:userId', async (req, res) => {
  try {
    await Joi.validate(req.params, deleteAvatarSchema)
  } catch (error) {
    return res.status(400).send(error)
  }

  try {
    let where = {};
    if (req.body.type == 'Business'){
      where = { id: req.params.userId }
    }else{
      where = { user_id: req.params.userId }
    }
    const user = await models[req.body.type].findOne({
      where
    })

    if (!user) {
      return res.status(404).send({ error: 'This user does not exist' })
    }
    let id = user.id ? user.id : user.user_id;
    if (res.locals.user.id !== user.id) {
      return res.status(403).send()
    }

    await user.update({ avatar: null })
    await deleteObject(req.body.key)

    res.send()
  } catch (e) {
    res.status(500).send(e)
  }
})

/**
 * @api {post} /uploads/document/:userId/phoneNumber Update User Phone Number
 * @apiName Update User Phone Number
 * @apiGroup Uploads
 *
 * @apiParam {String} phone User's phone number
 *
 * @apiPermission Authorized
 */
router.post('/document/:userId/phoneNumber', async (req, res) => {
  try {
    await Joi.validate(req.params.userId, Joi.number().greater(0).required())
    await Joi.validate(req.body.phone, Joi.string().required())
  } catch (error) {
    return res.status(400).send(error)
  }

  try {
    const user = await models.User.findOne({
      where: {
        user_id: req.params.userId
      }
    })

    if (res.locals.user.id !== user.user_id) {
      return res.status(403).send()
    }

    await checkRelatedNeeds(req)
    await user.update({ phone_number: req.body.phone })
    res.send(getTrimmedUser(user))
  } catch (error) {
    res.status(400).send({ error })
  }
})

const documentSchema = Joi.object().keys({
  userId: Joi.number().greater(0).required(),
  type: Joi.string().valid('utilityBills', 'driverLicense', 'idCard', 'phoneNumber')
})

/**
 * @api {post} /uploads/document/:userId/:type Update User Document By Type
 * @apiName Update User Document By Type
 * @apiGroup Uploads
 *
 * @apiParam (Query) {String="utilityBills", "driverLicense", "idCard"} type
 * @apiParam (FormData) {Binary} file Image
 * @apiParam (FormData) {String="utilityBills", "driverLicense", "idCard"} type
 * @apiParam (FormData) {String} (key) Image Key
 *
 * @apiPermission Authorized
 */
router.post('/document/:userId/:type', async (req, res) => {
  try {
    await Joi.validate(req.params, documentSchema)
  } catch (e) {
    return res.status(400).send(e)
  }

  const user = await models.User.findOne({
    where: { user_id: req.params.userId }
  })

  if (!user) {
    return res.status(404).send({ error: 'This user does not exist' })
  }

  if (res.locals.user.id !== user.user_id) {
    return res.status(403).send()
  }

  try {
    await checkRelatedNeeds(req)

    upload(req, res, async (err) => {
      if (err) throw err

      await user.update({ [req.params.type]: req.file.location })
      res.send(getTrimmedUser(user))
    })
  } catch (error) {
    res.status(403).send({ error })
  }
})

/**
 * @api {delete} /uploads/document/:userId/:type Delete User Document By Type
 * @apiName Delete User Document By Type
 * @apiGroup Uploads
 *
 * @apiParam (Query) {String="utilityBills", "driverLicense", "idCard"} type
 * @apiPermission Authorized
 */
router.delete('/document/:userId/:type', async (req, res) => {
  try {
    await Joi.validate(req.params, documentSchema)
  } catch (e) {
    return res.status(400).send(e)
  }

  try {
    await checkRelatedNeeds(req)

    const user = await models.User.findOne({
      where: {
        user_id: req.params.userId
      }
    })

    if (res.locals.user.id !== user.user_id) {
      return res.status(403).send()
    }

    if (!user) {
      return res.status(404).send({ error: 'User does not exist' })
    }

    // type - one of: driverLicense, utilityBills, phoneNumber, idCard
    await user.update({ [req.params.type]: null })

    if (req.params.type === 'phoneNumber') {
      res.send(getTrimmedUser(user))
    } else {
      await deleteObject(req.body.key)
      res.send(getTrimmedUser(user))
    }
  } catch (error) {
    res.status(400).send({ error })
  }
})

async function checkRelatedNeeds (req) {
  const needs = await models.Need.findAll({
    where: {
      user_id: req.params.userId
    }
  })

  const aliases = {
    phoneNumber: 'Phone number',
    idCard: 'Id card',
    utilityBills: 'Utility bills',
    driverLicense: 'Driver license'
  }

  const type = req.params.type || 'phoneNumber'

  // Return the related need
  const needWithDocument = needs.find(need => {
    const docName = aliases[type]
    
    return need.documents.includes(docName)
  })

  if (needWithDocument) {
    throw `Your need "${needWithDocument.title}" contains this document. Please, remove the need first.`
  }
}

function getTrimmedUser (user) {
  let newUser = { ...user.dataValues }
  const keysToDelete = ['createdAt', 'updatedAt', 'password', 'refreshToken']

  keysToDelete.forEach(key => delete newUser[key])

  return newUser
}

module.exports = router
