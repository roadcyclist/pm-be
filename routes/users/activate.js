const express = require('express')
const Joi = require('joi')
const router = express.Router()
const models = require('../../db/models')
const { sendWelcomeEmail } = require('../../services/email')
const loginUrl = `${process.env.URL}/auth/login`

async function destroyActivation (req, res) {
  await models.Activation.destroy({
    where: {
      activationId: req.query.activationId,
      userId: req.query.userId
    }
  })

  return res.redirect(loginUrl + '?activated=true')
}

const activationSchema = Joi.object().keys({
  userId: Joi.string().required(),
  activationId: Joi.string().required()
})

/**
 * @api {get} /user/activate Activate User
 * @apiName Activate User
 * @apiGroup Users
 *
 * @apiParam {String} activationId Query param
 * @apiParam {String} userId Query param
 *
 * @apiPermission Public
 */
router.get('/', async (req, res) => {
  try {
    await Joi.validate(req.query, activationSchema)
  } catch (e) {
    res.status(400).send(e)
  }

  const { userId, activationId } = req.query
  const activation = await models.Activation.findOne({
    where: { activationId }
  })

  if (!activation) {
    return res.redirect(loginUrl)
  }

  const personType = activation.userRole[0].toUpperCase() + activation.userRole.slice(1)

  try {
    let person;
    if(personType == "Business"){
      person = await models[personType].findOne({
        where: { id: userId }
      })
    }else{
      person = await models[personType].findOne({
        where: { user_id: userId }
      })
    }
    

    await person.update({ emailConfirmed: true })
    await destroyActivation(req, res)

    if (person.userRole === 'business') {
      await sendWelcomeEmail(person)
    }
  } catch (e) {
    res.redirect(loginUrl + '?activated=false')
  }
})

module.exports = router
