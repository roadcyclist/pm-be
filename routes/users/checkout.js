const express = require('express')
const Joi = require('joi')
const models = require('../../db/models/index')
const router = express.Router()
const { sendNeedPurchasedEmail, sendNeedDataEmail } = require('../../services/email')
const { getObject } = require('../../services/amazon')
const { verifyToken } = require('../../services/auth')
const stripe = require('stripe')(process.env.STRIPE_KEY)

router.use(verifyToken())

const singleSchema = Joi.object().keys({
  price: Joi.number().required(),
  token: Joi.object().required()
})

/**
 * @api {post} /checkout/single/:userId/:needId Buy Single Need
 * @apiName Buy Single Need
 * @apiGroup Checkout
 *
 * @apiParam {Object} token Stripe token object
 * @apiParam {Number} price Need price
 * @apiPermission Authorized Business
 */
router.post('/single/:userId/:needId', async (req, res) => {
  // const token = await stripe.tokens.create({
  //   card: {
  //     number: '4242424242424242',
  //     exp_month: 2,
  //     exp_year: 2022,
  //     cvc: '314',
  //   },
  // });
  try {
    await Joi.validate(req.body, singleSchema)
  } catch (error) {
    return res.status(400).send({ error })
  }

  try {
    if (res.locals.user.id !== +req.params.userId) {
      return res.status(403).send()
    }

    await stripe.charges.create({
      amount: req.body.price * 100, // converting cents to dollars
      currency: 'usd',
      description: `Single need ${req.params.needId} purchase by user ${req.params.userId}`,
      source: req.body.token.id
    })

    await purchaseNeed(req.params.needId, req.params.userId)

    return res.send({ message: 'Your need has been purchased! Check your e-mail.' })
  } catch (error) {
    res.status(400).send({ error })
  }
})

const multipleSchema = Joi.object().keys({
  price: Joi.number().required(),
  token: Joi.object().required(),
  needs: Joi.array().required()
})

/**
 * @api {post} /checkout/multiple/:userId/ Buy Multiple Needs
 * @apiName Buy Multiple Need
 * @apiGroup Checkout
 *
 * @apiParam {Object} token Stripe token object
 * @apiPermission Authorized Business
 */
router.post('/multiple/:userId', async (req, res) => {
  // const token = await stripe.tokens.create({
  //   card: {
  //     number: '4242424242424242',
  //     exp_month: 2,
  //     exp_year: 2022,
  //     cvc: '314',
  //   },
  // });
  try {
    await Joi.validate(req.body, multipleSchema)
  } catch (error) {
    res.status(400).send({ error })
  }

  try {
    if (res.locals.user.id !== +req.params.userId) {
      return res.status(403).send()
    }

    await stripe.charges.create({
      amount: req.body.price * 100,
      currency: 'usd',
      description: 'Multiple need purchase',
      source: req.body.token.id
    })

    // purchase each need separately
    for (const need of req.body.needs) {
      await purchaseNeed(need.id, req.params.userId)
    }

    res.send({ message: 'Needs have been successfully purchased and sent to your email.' })
  } catch (error) {
    res.status(500).send({ error })
  }
})

async function purchaseNeed (needId, buyerId) {
  try {
    const [need, buyer] = await Promise.all([
      models.Need.findOne({
        where: { id: needId }
      }),
      models.Business.findOne({
        where: { id: buyerId }
      })
    ])
    
    if(!(buyer.boughtNeedIds == null || buyer.boughtNeedIds == [])){
      if (buyer.boughtNeedIds.includes(need.id)) {
        throw `You have already bought Need '${need.name}'`
      }
    }
    
    // find need seller
    const user = await models.User.findOne({
      where: {
        user_id: need.user_id
      }
    })
    
    const needData = need.documents.map(documentType => {
      let result = {
        type: documentType,
        value: ''
      }
      
      switch (documentType) {
        case 'Phone number':
          result.value = user.phoneNumber
          break

        case 'Id card':
          result.value = user.idCard
          break

        case 'Driver license':
          result.value = user.driverLicense
          break

        case 'Utility bills':
          result.value = user.utilityBills
          break
      }

      return result
    })
    
    try {
      // if it doesn't exist, destroy the need
      await checkDocumentExistence(needData)
    } catch (err) {
      await need.destroy()
      throw err
    }

    let boughtNeedIds = buyer.boughtNeedIds?[...buyer.boughtNeedIds]:[];
    boughtNeedIds.push(need.id)
    let balance = parseFloat(user.balance) + need.price;
    await Promise.all([
      user.update({ balance: balance }), // money which goes to the user
      buyer.update({ boughtNeedIds })
    ])

    sendNeedPurchasedEmail(user, need)
    sendNeedDataEmail(buyer, need, needData)
  } catch (err) {
    throw err
  }
}

async function checkDocumentExistence (needData) {
  try {
    for (const need of needData) {
      if (need.type !== 'Phone number') {
        const key = need.value.split('.com/')[1]
        await getObject(key)
      }
    }
  } catch (err) {
    throw err
  }
}

module.exports = router
