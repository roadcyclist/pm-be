const express = require('express')
const router = express.Router()

const user = require('./user')
const register = require('./register')
const activation = require('./activate')
const uploads = require('./uploads')
const checkout = require('./checkout')

router.use('/register', register)
router.use('/checkout', checkout)
router.use('/activate', activation)
router.use('/uploads', uploads)
router.use('/', user)

module.exports = router
