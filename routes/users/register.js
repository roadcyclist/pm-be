const express = require('express')
const Joi = require('joi')
const bcrypt = require('bcrypt')
const crypto = require('crypto');
const router = express.Router()

const models = require('../../db/models/index')
const auth = require('../../services/auth')

async function checkEmailAvailability (email) {
  const where = { email }

  const [user, business] = await Promise.all([
    models.User.findAll({ where }),
    models.Business.findAll({ where })
  ])

  if (user.length || business.length) {
    throw { message: 'Email already in use', status: 400 }
  }
}
// const AES_METHOD = 'aes-256-cbc';

//let secret = 'lbwyBzfgzUIvXZFShJuikaWvLJhIVq36'; // Must be 256 bytes (32 characters)
// function makeSecret (){
//   return crypto.randomBytes(16).toString('hex');
// }


function encrypt(text,secret) {
    if (process.versions.openssl <= '1.0.1f') {
        throw new Error('OpenSSL Version too old, vulnerability to Heartbleed')
    }

    console.log(process.env.PASSWORD_IV_LENGTH);
    let iv = crypto.randomBytes(16);
    console.log(iv.toString('hex'));
    console.log(process.env.PASSWORD_AES_METHOD)
    let cipher = crypto.createCipheriv(process.env.PASSWORD_AES_METHOD, new Buffer(secret), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

const userSchema = Joi.object().keys({
  email: Joi.string().email(),
  birthday: Joi.string().required(),
  password: Joi.string().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required()
})

/**
 * @api {post} /user/register/user Register User
 * @apiName Register User
 * @apiGroup Auth
 *
 * @apiParam {String} email User email
 * @apiParam {String} birthday YYYY-MM-DD
 * @apiParam {String} firstName
 * @apiParam {String} lastName
 * @apiParam {String} password Must contain only alphanumeric characters with 1 uppercase, 1 lowercase, 1 number and be at least 8 characters long.
 *
 * @apiPermission Public
 */
router.post('/user', async (req, res) => {
  try {
    await Joi.validate(req.body, userSchema)
  } catch (error) {
    return res.status(400).send({ error })
  }

  try {
    await checkEmailAvailability(req.body.email)
    const birthDate = new Date(req.body.birthday)
    const now = new Date()

    if (now.getFullYear() - birthDate.getFullYear() < 18) {
      return res.status(400).send({ error: 'You should be at least 18 years old to register' })
    }
    // let secret = makeSecret();
    const hashedPass = encrypt(req.body.password, process.env.PASSWORD_SECRET);
    const user = await models.User.create({
      password: hashedPass,
      email: req.body.email,
      first_name: req.body.firstName,
      last_name: req.body.lastName,
      userRole: 'user',
      birthday: req.body.birthday,
      emailConfirmed: false
    })

    await auth.createActivation(req, user)

    res.send({ message: 'User has been successfully created! Please, verify your e-mail.' })
  } catch (error) {
    res.status(500).send({ error })
  }
})

const businessSchema = Joi.object().keys({
  email: Joi.string().email(),
  password: Joi.string().required(),
  name: Joi.string().required(),
  orgName: Joi.string().required()
})

/**
 * @api {post} /user/register/business Register Business
 * @apiName Register Business
 * @apiGroup Auth
 *
 * @apiParam {String} email User email
 * @apiParam {String} name
 * @apiParam {String} orgName
 * @apiParam {String} password Must contain only alphanumeric characters with 1 uppercase, 1 lowercase, 1 number and be at least 8 characters long.
 *
 * @apiPermission Public
 */
router.post('/business', async (req, res) => {
  try {
    await Joi.validate(req.body, businessSchema)
  } catch (error) {
    res.status(400).send({ error })
  }

  try {
    await checkEmailAvailability(req.body.email)

    const hashedPass = encrypt(req.body.password,process.env.PASSWORD_SECRET);
    const user = await models.Business.create({
      orgName: req.body.orgName,
      name: req.body.name,
      password: hashedPass,
      userRole: 'business',
      email: req.body.email,
      needsInCart: [],
      emailConfirmed: false
    })

    await auth.createActivation(req, user)

    res.send({ message: 'User has been successfully created! Please, verify your e-mail.' })
  } catch (error) {
    res.status(500).send({ error })
  }
})

module.exports = router
