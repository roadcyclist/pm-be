const express = require('express')
const router = express.Router()
const Joi = require('joi')

const { sendContactUsEmail } = require('../services/email')

const validationSchema = Joi.object().keys({
  name: Joi.string().required(),
  sender: Joi.string().email().required(),
  text: Joi.string().required()
})

/**
 * @api {post} /contact Contact Us
 * @apiName Contact Us
 * @apiGroup Contact
 *
 * @apiParam {String} name Username
 * @apiParam {String} sender E-mail
 * @apiParam {String} text User Info
 *
 * @apiSuccess {String} message Successful message
 */

router.post('/', async (req, res) => {
  try {
    await Joi.validate(req.body, validationSchema)
    await sendContactUsEmail(req.body.name, req.body.sender, req.body.text)
    res.send({ message: 'Thanks for your input! We\'ll get to you as soon as possible!' })
  } catch (error) {
    res.status(400).send({ error })
  }
})

module.exports = router
