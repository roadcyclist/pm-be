const express = require('express')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const Joi = require('joi')
const jwt = require('jsonwebtoken')

const models = require('../db/models/index')
const auth = require('../services/auth')
const email = require('../services/email')
const { sendPasswordRecoveryEmail } = require('../services/email')

const router = express.Router()

const validationSchema = Joi.object().keys({
  password: Joi.string().required(),
  email: Joi.string().email().required()
})

// const AES_METHOD = 'aes-256-cbc';
// const IV_LENGTH = 16; // For AES, this is always 16, checked with php

//let secret = 'lbwyBzfgzUIvXZFShJuikaWvLJhIVq36'; // Must be 256 bytes (32 characters)
function encrypt(text,secret) {
  if (process.versions.openssl <= '1.0.1f') {
      throw new Error('OpenSSL Version too old, vulnerability to Heartbleed')
  }

  console.log(process.env.PASSWORD_IV_LENGTH);
  let iv = crypto.randomBytes(16);
  console.log(iv.toString('hex'));
  console.log(process.env.PASSWORD_AES_METHOD)
  let cipher = crypto.createCipheriv(process.env.PASSWORD_AES_METHOD, new Buffer(secret), iv);
  let encrypted = cipher.update(text);

  encrypted = Buffer.concat([encrypted, cipher.final()]);

  return iv.toString('hex') + ':' + encrypted.toString('hex');
}




function decrypt(text,secret) {
  let textParts = text.split(':');
  let iv = new Buffer(textParts.shift(), 'hex');
  let encryptedText = new Buffer(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(process.env.PASSWORD_SECRET), iv);
  let decrypted = decipher.update(encryptedText);

  decrypted = Buffer.concat([decrypted, decipher.final()]);

  return decrypted.toString();
}

/**
 * @api {post} /auth/login Login
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {String} password User's password
 * @apiParam {String} email User's email
 *
 * @apiSuccess {String} accessToken
 * @apiSuccess {String} refreshToken
 * @apiSuccess {Number} expiresIn Access token expiration time in seconds
 */
router.post('/login', async (req, res) => {
  const errorMessage = { error: 'Your credentials are invalid' }
  try {
    await Joi.validate(req.body, validationSchema)
  } catch (e) {
    return res.status(400).send(errorMessage)
  }
  const where = { email: req.body.email }
  const [user, business] = await Promise.all([
    models.User.findOne({ where }),
    models.Business.findOne({ where })
  ])
  // Not found with such email
  if (!user && !business) {
    return res.status(400).send(errorMessage)
  }
  const person = user || business
  if (person.isDeleted) return res.status(403).send({ error: 'Your account have been deleted by administrator' })
  // if (!bcrypt.compareSync(req.body.password, person.password)) {
    console.log(decrypt(person.password));
  if(!(req.body.password == decrypt(person.password))){
    res.status(400).send(errorMessage)
    // User is not verified
  } else if (!person.emailConfirmed) {
    res.status(400).send({ error: 'Verify your email' })
  } else {
    const authData = await auth.generateTokens(person)
    const commonFields = {
      ...authData,
      avatar: person.avatar,
      userType: user ? 'user' : 'business',
      id: person.id,
      email: person.email
    }
    // User response
    if (user) {
      res.status(200).json({
        ...commonFields,
        name: user.firstName,
        phoneNumber: user.phoneNumber,
        utilityBills: user.utilityBills,
        idCard: user.idCard,
        driverLicense: user.driverLicense,
        confirmed: user.confirmed,
        confirmationImage: user.confirmationImage,
        balance: user.balance
      })
      
      // Business response
    } else {
      res.status(200).json({
        ...commonFields,
        name: business.name,
        needsInCart: business.needsInCart
      })
    }
  }
})

/**
 * @api {post} /auth/logout Logout user
 * @apiName Logout
 * @apiGroup Auth
 *
 * @apiParam {String} token User's token
 *
 */
router.post('/logout', async (req, res) => {
  try {
    await Joi.validate(req.body, Joi.object().keys({
      token: Joi.string().required()
    }))
  } catch (e) {
    return res.status(400).send(e)
  }

  const accessToken = req.body.token
  const decoded = jwt.decode(accessToken)

  if (decoded) {
    const { id, email } = decoded
    const [user, business] = await Promise.all([
      models.User.findOne({ where: { user_id:id, email } }),
      models.Business.findOne({ where: { id, email } })
    ])

    const person = user || business

    if (person) {
      person.refreshToken = ''
      await person.save()
    }

    res.status(200).send()
  } else {
    res.status(400).send({ error: 'Invalid token' })
  }
})

/**
 * @api {post} /auth/refresh Renew Refresh Token
 * @apiName Refresh
 * @apiGroup Auth
 *
 * @apiParam {String} refreshToken Valid refresh token
 *
 * @apiSuccess {String} accessToken
 * @apiSuccess {String} refreshToken
 * @apiSuccess {Number} expiresIn Access token expiration time in seconds
 *
 * @apiPermission Authorized user
 */
router.post('/refresh', async (req, res) => {
  try {
    await Joi.validate(req.body.refreshToken, Joi.string().required())

    const { refreshToken } = req.body
    const errorMessage = { error: 'Not a valid refresh token' }

    jwt.verify(refreshToken, process.env.TOKEN_SECRET, async (err, decoded) => {
      if (err) {
        res.status(401).send(errorMessage)
      } else if (decoded) {
        const { id, email } = decoded
        const where = { id, email }

        const [user, business] = await Promise.all([
          models.User.findOne({where:{ user_id:id, email }}),
          models.Business.findOne({ where })
        ])

        const person = user || business
        if (person.refreshToken !== refreshToken) {
          res.status(401).json(errorMessage)
        } else {
          const tokens = await auth.generateTokens(person)
          res.send(tokens)
        }
      }
    })
  } catch (e) {
    res.status(400).send(e)
  }
})

const generatePass = () => {
  let randomPassword = ''
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  for (let i = 0; i < 10; i++) {
    randomPassword += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return randomPassword
}

/**
 * @api {post} /auth/reset-password Reset Password
 * @apiName Reset Password
 * @apiGroup Auth
 *
 * @apiParam {String} email User email
 *
 * @apiPermission Authorized user
 */
router.post('/reset-password', async (req, res) => {
  try {
    await Joi.validate(req.body.email, Joi.string().email())
  } catch (e) {
    return res.status(400).send(e)
  }

  const [user, business] = await Promise.all([
    models.User.findOne({
      where: { email: req.body.email }
    }),
    models.Business.findOne({
      where: { email: req.body.email }
    })
  ])

  const person = user || business

  if (!person) return res.status(404).send({ error: 'User not found' })

  const newPass = generatePass()
  console.log(newPass);
  let newHashPass = encrypt(newPass,process.env.PASSWORD_SECRET);
  try {
    const type = person.userRole[0].toUpperCase() + person.userRole.slice(1)
  
    await models[type].update({ password: newHashPass }, { where: { email: person.email } })
    await sendPasswordRecoveryEmail(req.body.email, newPass)
    res.status(200).send({ message: 'Successfully changed your password' })
  } catch (e) {
    res.status(500).send(e)
  }
  // bcrypt.hash(newPass, 10, async (err, hash) => {
  //   if (err) res.status(500).send(err)

  //   try {
  //     const type = person.userRole[0].toUpperCase() + person.userRole.slice(1)

  //     await models[type].update({ password: hash }, { where: { email: person.email } })
  //     await sendPasswordRecoveryEmail(req.body.email, newPass)
  //     res.status(200).send({ message: 'Successfully changed your password' })
  //   } catch (e) {
  //     res.status(500).send(e)
  //   }
  // })
})

/**
 * @api {post} /auth/resend-activation Resend Activation
 * @apiName Resend Activation
 * @apiGroup Auth
 *
 * @apiParam {String} email User email
 *
 * @apiPermission Public
 */
router.post('/resend-activation', async (req, res) => {
  try {
    await Joi.validate(req.body.email, Joi.string().email().required())

    const [user, business] = await Promise.all([
      models.User.findOne({ where: { email: req.body.email } }),
      models.Business.findOne({ where: { email: req.body.email } })
    ])
    const person = user || business

    if (!person) return res.status(404).send({ error: 'Your email is not registered in the system' })

    const activation = await models.Activation.findOne({
      where: { userId: person.id, userRole: person.userRole }
    })

    if (!activation) return res.status(404).send({ error: 'You have already been activated' })

    await email.sendConfirmEmail(req.body.email, person.id, activation.activationId)
    res.send({ message: 'Please, check your email' })
  } catch (e) {
    res.status(500).send(e)
  }
})

module.exports = router
