const express = require('express')
const path = require('path')
const fs = require('fs')

const user = require('./users')
const auth = require('./auth')
const needs = require('./needs')
const admin = require('./admin/index')
const contact = require('./contact')

const router = express.Router()

router.use('/user', user)
router.use('/auth', auth)
router.use('/needs', needs)
router.use('/admin', admin)
router.use('/contact', contact)

/**
 * @api {get} /static/:type Get Static Content
 * @apiName Get Static Content
 * @apiGroup Other
 *
 * @apiParam {String} type landing (json) || privacy (html) || terms (html)
 *
 * @apiPermission Public
 *
 */
router.get('/static/:type', (req, res) => {
  if (req.params.type === 'landing') {
    fs.readFile(
      path.join(__dirname, '../static.json'),
      (e, data) => {
        if (e) res.status(500).send()

        return res.send(data)
      })
  } else {
    res.sendFile(path.join(__dirname, `../views/${req.params.type}.html`))
  }
})

/* Ping */
router.get('/', (req, res) => {
  res.status(200).send()
})

module.exports = router
