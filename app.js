const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const compression = require('compression')

const api = require('./routes')
const app = express()

app.use(cors({
  origin: [/http:\/\/localhost(:\d*)?/, /(https:\/\/)([a-z]*)(\.?cryptoveil\.io)/]
}))
app.use('/public', express.static('public'))
app.use(compression())
app.use(helmet())
app.use(morgan('tiny'))
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }))
app.use(bodyParser.json({ limit: '50mb' }))

app.get('/', (req, res) => {
  res.json({ status: 'OK', time: Date.now() })
})

app.use('/', api)

module.exports = app
