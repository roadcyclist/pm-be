#!/bin/bash
docker build -t pmbe .

docker stop pmbe_container
docker rm pmbe_container
docker run -p 3000:3000 --name=pmbe_container pmbe
